const URL =  'http://localhost:8080/items/items';

async function getItems(url) {
    const resp = await fetch(url, {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": URL
        }
    }); 

    return resp;
}

async function postData(url, data) {
    const resp = await fetch(url, {
        method: "POST",
        mode: "cors",
        headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": URL
        },
        body: JSON.stringify(data)
    });
    return resp;
}

async function updateData(url, data) {
    const resp = await fetch(url, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": URL
        },
        body: JSON.stringify(data)
    });
    return resp;
}

async function deleteData(url, index) {
    const resp = await fetch(url + "/" + index, {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*"
        },
        body: URL + "/" + index
    });
    return resp;
} 

