function load() {
    headerLoad();
    footerLoad();
    getItems('http://localhost:8080/items/items').then(resp => resp.json()).then(data => this.saveAllItems(data));
    loadItems(getAllItems());    

    document.getElementById("allButton").addEventListener("click", allButton);
    document.getElementById("activeButton").addEventListener("click", activeButton);
    document.getElementById("completeButton").addEventListener("click", completeButton);
}

function loadItems(allItems) {
    let article = document.getElementById("itemsContainer");
    article.innerHTML = ``;

    if(allItems != []) {
        allItems.forEach((element, index) => {
            let aItem = document.createElement("li");

            if(element["status"] === "COMPLETED") {
                aItem.innerHTML = `
                    <input type="checkbox", id="itemcheck${index}", name="check", onclick="checkItem(this)" checked>
                    <span id="itemContent${index}" ondblclick="editText(event)" class="itemContentAfterCheck">${element.text}</span>
                    <div id="itemDelete${index}" onclick="deleteItem(this)" class="deleteButton"></div>`;
                aItem.setAttribute("id", `li-${index}`)
                aItem.setAttribute("class", "liAfterCheck");
                aItem.addEventListener("mouseover", mouseOverEvent);
                aItem.addEventListener("mouseout", mouseOutEvent);
            } else {
                aItem.innerHTML = `
                    <input type="checkbox", id="itemcheck${index}", name="check", onclick="checkItem(this)">
                    <span id="itemContent${index}" ondblclick="editText(event)">${element.text}</span>
                    <div id="itemDelete${index}" onclick="deleteItem(this)" class="deleteButton"></div>`;
                aItem.setAttribute("id", `li-${index}`);
                aItem.addEventListener("mouseover", mouseOverEvent);
                aItem.addEventListener("mouseout", mouseOutEvent);
            }
            article.appendChild(aItem);
        });
    } 
}

function headerLoad() {
    let header = document.getElementById("title");
    let headerTitle = document.createElement("span");
    headerTitle.innerHTML = `To Do Items`;
    headerTitle.setAttribute("class", "headerTitle");
    header.appendChild(headerTitle);

    let description = document.createElement("span");
    description.innerHTML = `Use this application to manage your to do items`;
    description.setAttribute("class", "description");
    header.appendChild(description);

    let inputAndAdd = document.createElement("span");
    inputAndAdd.innerHTML = `
    <input type="text" id="itemInput" onkeydown="addItemByEnter(event)" placeholder="Enter your to do item" class="itemInput">
    <button onclick="addItem()" class="addButton"></button>
    `;
    header.appendChild(inputAndAdd);
}

function footerLoad() {
    let btContainer = document.getElementById("btnContainer");
    let allButton = document.createElement("button");
    allButton.innerText = "ALL";
    allButton.setAttribute("class", "allButton");
    allButton.setAttribute("id", "allButton");
    btContainer.appendChild(allButton);

    let activeButton = document.createElement("button");
    activeButton.innerText = "Active";
    activeButton.setAttribute("class", "activeButton");
    activeButton.setAttribute("id", "activeButton");
    btContainer.appendChild(activeButton);

    let completeButton = document.createElement("button");
    completeButton.innerText = "Completed";
    completeButton.setAttribute("class", "completeButton");
    completeButton.setAttribute("id", "completeButton");
    btContainer.appendChild(completeButton);
}

function addItem() {
    postData('http://localhost:8080/items/items', {"text": document.getElementById("itemInput").value});
    
    let allItems = getAllItems();
    allItems.push({"id": allItems[allItems.length-1]["id"]+1, "text": document.getElementById("itemInput").value, "status": "ACTIVE"});

    loadItems(allItems);
    saveAllItems(allItems);
    document.getElementById("itemInput").value = "";
}
function addItemByEnter(e) {
    if(e.keyCode === 13) {
        addItem();
    }
} 

function checkItem(event) {
    let targetId = event.id;
    let index = targetId.substr(targetId.length-1);

    let itemDatas = getAllItems();
    let status = itemDatas[index]["status"] === "ACTIVE" ? "COMPLETED":"ACTIVE";
    updateData('http://localhost:8080/items/items', {"id": itemDatas[index]["id"], "text": itemDatas[index]["text"], "status": `${status}`});
    
    itemDatas[index]["status"] = status;
    loadItems(itemDatas);
    saveAllItems(itemDatas);
}

function deleteItem(event) {
    let targetId = event.id;
    let index = targetId.substr(targetId.length - 1);

    let itemDatas = getAllItems();
    let id = itemDatas[index]["id"];
    deleteData('http://localhost:8080/items/items', id);

    itemDatas.splice(index, 1);
    loadItems(itemDatas);
    saveAllItems(itemDatas);
}

function allButton() {
    let itemDatas = getAllItems();
    loadItems(itemDatas);
}

function activeButton() {
    let itemDatas = getAllItems();

    let itemsShow = itemDatas.filter((item) => item["status"] === "ACTIVE");
    loadItems(itemsShow);
}

function completeButton() {
    let itemDatas = getAllItems();
    let itemsShow = itemDatas.filter((item) => item["status"] === "COMPLETED");
    loadItems(itemsShow);
}

function mouseOverEvent(e) {
    let targetId = e.target.id;
    let index = targetId.substr(targetId.length-1);

    let deleteDiv = document.getElementById(`itemDelete${index}`);
    deleteDiv.setAttribute("class", "mouseOverDeleteButton");
}

function mouseOutEvent(e) {
    let targetId = e.target.id;
    let index = targetId.substr(targetId.length-1);

    let deleteDiv = document.getElementById(`itemDelete${index}`);
    deleteDiv.setAttribute("class", "deleteButton");
}

function editText(e) {
    let targetId = e.target.id;
    let index = targetId.substr(targetId.length - 1);
    
    let oldValue = e.target.innerHTML;
    e.target.innerHTML = `<input type="text" class="textEdit" autofocus="autofocus" onkeydown="editTextEnter(event)" id="inputItemContent-${index}" value=${oldValue}>`;
}

function editTextEnter(e) {
    let index = e.target.id.substr(e.target.id.length - 1);

    if(e.keyCode === 13) {
        let itemDatas = getAllItems();
        let newText = e.target.value;
        updateData('http://localhost:8080/items/items', {"id": itemDatas[index]["id"], "text": `${newText}`, "status": itemDatas[index]["status"]});

        itemDatas[index]["text"] = newText;
        loadItems(itemDatas);
        saveAllItems(itemDatas);
    }
}
